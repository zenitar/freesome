package com.freesome.web.controller;

import com.freesome.core.entity.SomeEntity;
import com.freesome.core.service.PersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/")
public class AppController {
    private final PersistenceService<SomeEntity<String>, String> persistenceService;

    @Autowired
    public AppController(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping
    public String home(Model model) {
        model.addAttribute("test", LocalDateTime.now().toString());
        return "home";
    }

    @GetMapping("test")
    public ResponseEntity getTest(@RequestParam String id) {
        SomeEntity testEntity = persistenceService.findById(id);
        if (testEntity == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(testEntity, HttpStatus.OK);
    }

    @GetMapping("tests")
    public ResponseEntity getTests() {
        Page page = persistenceService.findAll(PageRequest.of(0, 100));

        return new ResponseEntity(page, HttpStatus.OK);
    }

    @PostMapping("test")
    public ResponseEntity setTest(@RequestParam String value) {
        Object id = persistenceService.add(value);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @GetMapping("uni")
    public ResponseEntity stub() {
        return new ResponseEntity("{\n" +
                "    \"result\": 0,\n" +
                "    \"errMsg\": \"OK\",\n" +
                "    \"SESSION_INFO\": {\n" +
                "        \"SESSION_ID\": \"PRIMARY_TEST;250110207863893;1100000000005475\",\n" +
                "        \"SUBSCRIBER_ID\": \"250110207863893\",\n" +
                "        \"IP\": \"10.183.113.145\",\n" +
                "        \"IPV6\": \"2a02:1a0:21cf:b7ab::/64\",\n" +
                "        \"BS_ID\": \"25011BEEB302\",\n" +
                "        \"CREATE_TIME\": \"2018-07-26 07:29:24\",\n" +
                "        \"QOS_POLICY\": \"qos_policy_tst\",\n" +
                "        \"RULES\": [\n" +
                "            {\n" +
                "                \"RULE_ID\": \"Rule1\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"RULE_ID\": \"Rule2\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"RULE_ID\": \"Rule3\"\n" +
                "            }\n" +
                "        ],\n" +
                "        \"ATTRIBUTES\": {\n" +
                "            \"Session Attribute 1\": \"Primary test session\",\n" +
                "            \"Session Attribute 2\": \"Created for test\"\n" +
                "        },\n" +
                "        \"IMEISV\": 1122222203418444,\n" +
                "        \"RAT_TYPE\": \"EUTRAN\",\n" +
                "        \"REGION\": \"RUS_YOTA_STH_RST\",\n" +
                "        \"MACRO_REGION\": \"RUS_YOTA_NRW\",\n" +
                "        \"OPERATOR\": \"YOTA\",\n" +
                "        \"COUNTRY\": \"RU\",\n" +
                "        \"REGION_GROUPS\": [\n" +
                "            \"78\",\n" +
                "            \"MA6\",\n" +
                "            \"RUS_NRW\",\n" +
                "            \"RUS_NRW_SPB_LO\",\n" +
                "            \"RUS_YOTA_LIGHT64\"\n" +
                "        ],\n" +
                "        \"SGSN_IP\": \"10.50.20.21\",\n" +
                "        \"PLMN\": 25011,\n" +
                "        \"TAC\": 8888,\n" +
                "        \"BS_ATTRIBUTES\": {\n" +
                "            \"throughput\": \"00\",\n" +
                "            \"usercount\": \"2\"\n" +
                "        },\n" +
                "        \"SECONDARY_SESSIONS\": [\n" +
                "            {\n" +
                "                \"SESSION_ID\": \"SECONDARY_TEST;250110207863893;1100000000005475\",\n" +
                "                \"SUBSCRIBER_ID\": \"250110207863893\",\n" +
                "                \"IP\": \"10.183.113.145\",\n" +
                "                \"IPV6\": \"2a02:1a0:21cf:b7ab::/64\",\n" +
                "                \"BS_ID\": \"25011BEEB302\",\n" +
                "                \"CREATE_TIME\": \"2018-07-26 07:29:24\",\n" +
                "                \"QOS_POLICY\": \"qos_policy_tst_sec\",\n" +
                "                \"RULES\": [\n" +
                "                    {\n" +
                "                        \"RULE_ID\": \"Cell/C00/25011555555555\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"RULE_ID\": \"Cell_stats/25011555555555\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"RULE_ID\": \"SPB/LTE/Group6/RG0/6Month\"\n" +
                "                    }\n" +
                "                ],\n" +
                "                \"ATTRIBUTES\": {\n" +
                "                    \"Session Attribute 1\": \"Secondary test session\"\n" +
                "                }\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}", HttpStatus.OK);
    }

    @PostMapping("mq")
    public ResponseEntity mqStub(@RequestBody String body, @RequestHeader MultiValueMap<String, String> headers) {
        return new ResponseEntity(HttpStatus.OK);
    }
}
