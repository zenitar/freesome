package com.freesome.web;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.freesome.web.controller"})
public class WebConfiguration {
}
