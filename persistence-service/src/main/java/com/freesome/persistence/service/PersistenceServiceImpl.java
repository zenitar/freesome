package com.freesome.persistence.service;

import com.freesome.core.dao.TestDao;
import com.freesome.core.entity.SomeEntity;
import com.freesome.core.service.PersistenceService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public class PersistenceServiceImpl<E extends SomeEntity<K>, K> implements PersistenceService<E, K> {
    private final TestDao<E, K> testDao;

    public PersistenceServiceImpl(TestDao testDao) {
        this.testDao = testDao;
    }

    @Override
    public E findById(K id) {
        return testDao.findById(id).orElse(null);
    }

    @Override
    public Page<E> findAll(Pageable pageable) {
        return testDao.findAll(pageable);
    }

    @Override
    public K add(String fieldValue) {
        E testEntity = testDao.createTestEntity();
        testEntity.setTestField(fieldValue);
        testDao.saveAndFlush(testEntity);
        return testEntity.getId();
    }
}
