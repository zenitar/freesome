package com.freesome.persistence.service;

import com.freesome.core.dao.TestDao;
import com.freesome.core.service.PersistenceService;
import com.freesome.persistence.jdbc.JpaConfiguration;
import com.freesome.persistence.redis.RedisConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        JpaConfiguration.class,
        RedisConfiguration.class
})
public class PersistenceServiceConfiguration {
    @Bean
    public PersistenceService persistenceService(TestDao testDao) {
        return new PersistenceServiceImpl(testDao);
    }
}
