package com.freesome.core.entity;

import java.time.LocalDateTime;

public interface AuditEntity {
    LocalDateTime getCreatedDate();

    void setCreateDate(LocalDateTime createDate);

    LocalDateTime getModifiedDate();

    void setModifiedDate(LocalDateTime modifiedDate);
}
