package com.freesome.core.entity;

public interface SomeEntity<ID> {
    ID getId();
    void setTestField(String field);
}
