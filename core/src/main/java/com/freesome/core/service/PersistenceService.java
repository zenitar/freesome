package com.freesome.core.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersistenceService<E, K> {
    E findById(K id);
    Page<E> findAll(Pageable pageable);
    K add(String fieldValue);
}
