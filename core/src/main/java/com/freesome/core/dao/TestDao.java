package com.freesome.core.dao;


import com.freesome.core.entity.SomeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface TestDao<E extends SomeEntity<I>, I> {
    Optional<E> findById(I id);
    E saveAndFlush(E entity);
    E createTestEntity();
    Page<E> findAll(Pageable pageable);
}
