package com.freesome.acl.entity;


import com.freesome.core.entity.AuditEntity;

import java.io.Serializable;
import java.util.Set;

public interface User<G extends Group> extends AuditEntity, Serializable {
    User<G> createUser();

    String getLogin();

    void setLogin(String login);

    String getPassword();

    void setPassword(String password);

    String getEmail();

    void setEmail(String email);

    Set<G> getGroups();

    void setGroups(Set<G> groups);
}
