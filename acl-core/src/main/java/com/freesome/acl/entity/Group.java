package com.freesome.acl.entity;

import com.freesome.core.entity.AuditEntity;

import java.io.Serializable;
import java.util.Set;

public interface Group<U extends User> extends AuditEntity, Serializable {
    Group<U> createGroup();
    String getName();
    void setName(String name);
    Role getRole();
    void setRole(Role role);
    Set<U> getUsers();
    void setUsers(Set<U> users);
}
