package com.freesome.acl.entity;

import com.freesome.core.entity.AuditEntity;

import java.io.Serializable;
import java.util.Set;

public interface Role<G extends Group> extends AuditEntity, Serializable {
    Role<G> createRole();
    String getName();
    void setName(String name);
    int getWeight();
    void setWeight(int weight);
    Set<G> getGroups();
    void setGroups(Set<G> users);
}
