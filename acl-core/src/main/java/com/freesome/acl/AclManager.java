package com.freesome.acl;

import com.freesome.acl.entity.Role;
import com.freesome.acl.entity.User;
import com.freesome.acl.entity.Group;

public interface AclManager {
    boolean grantUserRole(User user, Role promotedRole);
    boolean authenticateUser(User user);
    boolean removeUser(User user);
    boolean createUser(User user);
    boolean createGroup(Group group);
    boolean removeGroup(Group group);
}
