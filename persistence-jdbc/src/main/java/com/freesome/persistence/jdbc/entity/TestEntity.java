package com.freesome.persistence.jdbc.entity;


import com.freesome.core.entity.SomeEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "TEST_TABLE", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
public class TestEntity implements SomeEntity<String> {
    public TestEntity() {
        id = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "TEST_COLUMN")
    private String testField;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTestField() {
        return testField;
    }

    @Override
    public void setTestField(String testField) {
        this.testField = testField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestEntity that = (TestEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(testField, that.testField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, testField);
    }
}
