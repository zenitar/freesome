package com.freesome.persistence.jdbc.repository;


import com.freesome.core.dao.TestDao;
import com.freesome.persistence.jdbc.entity.TestEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface TestRepository extends JpaRepository<TestEntity, String>, TestDao<TestEntity, String> {
    @Override
    Optional<TestEntity> findById(String aLong);

    @Override
    TestEntity saveAndFlush(TestEntity entity);

    @Override
    default TestEntity createTestEntity() {
        return new TestEntity();
    }

    @Override
    Page<TestEntity> findAll(Pageable pageable);
}
