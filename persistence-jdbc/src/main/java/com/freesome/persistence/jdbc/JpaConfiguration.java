package com.freesome.persistence.jdbc;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Profile("jdbc")
@Configuration
@EnableJpaRepositories("com.freesome.persistence.jdbc.repository")
@EntityScan("com.freesome.persistence.jdbc.entity")
public class JpaConfiguration {
}
