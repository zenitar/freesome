package com.freesome.persistence.redis.entity;

import com.freesome.core.entity.SomeEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.Objects;

@RedisHash("TEST")
public class TestRedisEntity implements SomeEntity<String>, Serializable {
    @Id
    private String id;
    private String testField;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setTestField(String testField) {
        this.testField = testField;
    }

    public String getTestField() {
        return testField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestRedisEntity that = (TestRedisEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(testField, that.testField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, testField);
    }
}
