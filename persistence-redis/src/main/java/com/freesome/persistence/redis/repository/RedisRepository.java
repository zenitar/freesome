package com.freesome.persistence.redis.repository;

import com.freesome.core.dao.TestDao;
import com.freesome.persistence.redis.entity.TestRedisEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public interface RedisRepository extends CrudRepository<TestRedisEntity, String>, TestDao<TestRedisEntity, String> {
    @Override
    Optional<TestRedisEntity> findById(String s);

    @Override
    default TestRedisEntity saveAndFlush(TestRedisEntity entity) {
        return save(entity);
    }

    @Override
    default Page<TestRedisEntity> findAll(Pageable pageable) {
        return new PageImpl<>(StreamSupport.stream(findAll().spliterator(), false).collect(Collectors.toList()));
    }

    default TestRedisEntity createTestEntity() {
        return new TestRedisEntity();
    }
}
