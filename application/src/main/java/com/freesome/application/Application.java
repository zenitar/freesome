package com.freesome.application;

import com.freesome.persistence.service.PersistenceServiceConfiguration;
import com.freesome.web.WebConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
        PersistenceServiceConfiguration.class,
        WebConfiguration.class
})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
