package com.freesome.acl.jdbc.entity;

import com.freesome.acl.entity.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "GROUP", uniqueConstraints = {@UniqueConstraint(columnNames = "NAME")})
public class Group implements com.freesome.acl.entity.Group<User> {
    private static final long serialVersionUID = -996848239941781155L;

    @Override
    public com.freesome.acl.entity.Group<User> createGroup() {
        return new Group();
    }

    @Id
    @GeneratedValue
    private long groupId;

    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "roleId")
    private Role role;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<User> users;

    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;

    @Column(name = "MODIFIED_DATE")
    private LocalDateTime modifiedDate;

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public Set<User> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public LocalDateTime getCreatedDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    @Override
    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }

    @Override
    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
